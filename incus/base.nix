{ config, pkgs, lib, ... }:
{
  time.timeZone = "America/Sao_Paulo";
  hardware.graphics.enable = true;

  programs.zsh = {
    enable = true;
    enableCompletion = true;
  };

  environment.systemPackages = with pkgs; [
    neovim
    git
  ];

  fonts = {
    fontDir.enable = true;
    enableGhostscriptFonts = true;
    packages = with pkgs; [
      intel-one-mono
      noto-fonts
      noto-fonts-emoji
      liberation_ttf
    ];
  };

  users.users.user = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "video" ];
    shell = pkgs.zsh ;
    packages = with pkgs; [
      clang
      tmux
      wget
      zsh-completions
      magic-wormhole
      zoxide
      poppler_utils
      lua53Packages.lua
      tree-sitter
      detox
      direnv
      unzip
      eza
      bat
      fd
      procs
      ripgrep
      fzf
      # GUI stuff
      rofi
      gtk3-x11
      alacritty
      lxqt.lxqt-policykit
      ripdrag
      dunst
      firefox
      xclip
    ];
  };
}
