#!/usr/bin/env sh

echo "Creating custom profile..."
incus profile create custom

echo "Configuring custom profile..."
cat profile.yml | incus profile edit custom
