#!/usr/bin/env sh
# $1 = project name
# $2 = network name
# $3 = ipv4 part


echo "creating project"
incus project create $1

echo "adding configuration to new project"
incus profile show default --project default | incus profile edit default --project $1

echo "creating network"
incus network create $2 ipv6.address=none ipv4.address=192.168.$3.254/24 ipv4.nat=true

echo "configuring network for new project"
incus profile device set default eth0 network=$2 --project $1

incus project list
