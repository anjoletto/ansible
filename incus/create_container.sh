#!/usr/bin/env sh

#echo "creating container..."
incus launch images:nixos/unstable $1 -c security.nesting=true --profile default --profile custom

echo "copying base configuration to container..."
incus file push ./base.nix $1/etc/nixos/
incus exec $1 -- sed -i '/.\/lxd.nix/ a \\t.\/base.nix' /etc/nixos/configuration.nix
#incus exec $1 -- nixos-rebuild --upgrade switch
