# Ops

Everything that I use for system configuration

Run with:
- primary device configuration: `ansible-pull -U https://gitlab.com/anjoletto/ops -t primary -i localhost, --purge`
- secondary device configuration: `ansible-pull -U https://gitlab.com/anjoletto/ops -t secondary -i localhost, --purge`
- incus' NixOS container configuration:
    1. run `./incus/install_profile.sh` script
    2. run `./incus/create_container.sh` script with a container name
    3. run `nixos-rebuild --upgrade switch` inside container
    4. run `ansible-pull -U https://gitlab.com/anjoletto/ops -t container -i localhost, --purge` inside the container
- When using alpine as root, also run `ansible-pull -U https://gitlab.com/anjoletto/ops -t primary -i localhost, --purge root.yml` to install some packages
